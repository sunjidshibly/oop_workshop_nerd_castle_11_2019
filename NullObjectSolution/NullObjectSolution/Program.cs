﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullObjectSolution
{
    class Program
    {
        static void Main(string[] args)
        {
            DB employeeDb = new DB();
            Employee anEmployee = employeeDb.GetEmployee("002654");
            Console.WriteLine(anEmployee.Pay());
            Console.ReadKey();
        }
    }
}
