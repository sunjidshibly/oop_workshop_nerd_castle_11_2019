﻿using System.Collections.Generic;

namespace NullObjectSolution
{
    class DB
    {
        private static List<Employee> employees = new List<Employee>();

        static DB()
        {
            employees.Add(new Employee("001", 25000));
            employees.Add(new Employee("002", 23000));
            employees.Add(new Employee("003", 55000));
            employees.Add(new Employee("004", 37000));
        }

        public Employee GetEmployee(string code)
        {
            foreach (Employee anEmployee in employees)
            {
                if (anEmployee.EmployeeCode == code)
                {
                    return anEmployee;
                }
            }
            return Employee.GetNullEmployee();
        }
    }
}
