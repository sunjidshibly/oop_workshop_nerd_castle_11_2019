﻿namespace NullObjectSolution
{
    internal class NullEmployee : Employee
    {
        public NullEmployee(string employeeCode, double amountToBePaid) :base(employeeCode
            , amountToBePaid)
        {
            
        }

        public override string Pay()
        {
            return "Nothing to pay, since employee is not found";
        }
    }
}