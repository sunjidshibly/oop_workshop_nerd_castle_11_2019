﻿namespace NullObjectSolution
{
    class Employee
    {
        public string EmployeeCode { set; get; }
        public double AmountToBePaid { set; get; }
        public Employee(string employeeCode, double amountToBePaid)
        {
            EmployeeCode = employeeCode;
            AmountToBePaid = amountToBePaid;
        }

        public static Employee GetNullEmployee()
        {
            return new NullEmployee("000", 0);
        }

        public virtual string Pay()
        {
            return "Paid";
        }
    }
}
