﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace W9_ReturnObjectFromMethod
{
    class Student
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string RegNo { get; set; }
    }
}
