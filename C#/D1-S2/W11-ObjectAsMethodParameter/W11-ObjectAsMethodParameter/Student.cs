﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace W11_ObjectAsMethodParameter
{
    class Student
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
