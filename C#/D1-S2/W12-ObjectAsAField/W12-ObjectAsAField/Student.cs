﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W12_ObjectAsAField
{
    class Student
    {
        public string Name {set; get;}
        public string Email {set; get;}
        public Result ResultSheet {set; get;}
    }
}
