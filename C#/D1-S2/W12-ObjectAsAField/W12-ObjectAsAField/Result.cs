﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace W12_ObjectAsAField
{
    class Result
    {
        public string Grade { set; get; }
        public int PublishedYear { set; get; }
    }
}
