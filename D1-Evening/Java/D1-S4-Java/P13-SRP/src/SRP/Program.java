package SRP;

public class Program {
    public static void main(String[] args) {
        Person aPerson = new Person();
            aPerson.setFirstName("Fahim");
            aPerson.setMiddleName("Hasan");
            aPerson.setLastNam("Rana");

            aPerson.saveDataInTextFile();
            aPerson.getMyFullName();
            aPerson.getFullReverseName();
    }
}
