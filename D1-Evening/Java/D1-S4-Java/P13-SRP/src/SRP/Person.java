package SRP;

public class Person {

    private String FirstName;
    private String MiddleName;
    private String LastNam;
    private String fullName;

    public void getMyFullName() {
        fullName = FirstName + " " + MiddleName + " " + LastNam;
        System.out.println(fullName);
    }

    public void getFullReverseName() {
        String reverseName = "";
        for (int index = fullName.length() - 1; index >= 0; index--) {
            reverseName += fullName.charAt(index);
        }
        System.out.println(reverseName);
    }

    public void saveDataInTextFile() {
        // Code for savings data in text file 
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getLastNam() {
        return LastNam;
    }

    public void setLastNam(String LastNam) {
        this.LastNam = LastNam;
    }

}
