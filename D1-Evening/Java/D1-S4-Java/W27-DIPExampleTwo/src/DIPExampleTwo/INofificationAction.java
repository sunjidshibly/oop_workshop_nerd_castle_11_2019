package DIPExampleTwo;

public interface INofificationAction {
    void actOnNotification(String message);
    
}
