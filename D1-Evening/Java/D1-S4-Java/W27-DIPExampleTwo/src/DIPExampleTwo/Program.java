package DIPExampleTwo;

public class Program {
    public static void main(String[] args) {
        AppPoolWatcher poolWatcher = new AppPoolWatcher();
            poolWatcher.setAction(new EmailSender());
            poolWatcher.notify("Memory overflow");

            poolWatcher.setAction(new EventLogWriter());
            poolWatcher.notify("Too many applictions");
    }
}
