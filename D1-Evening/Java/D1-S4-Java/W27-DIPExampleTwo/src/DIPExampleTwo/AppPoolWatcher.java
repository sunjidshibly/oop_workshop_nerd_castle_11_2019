package DIPExampleTwo;

public class AppPoolWatcher {
    private INofificationAction Action;

        // This function will be called when the app pool has problem
        public void notify(String message)
        {
            if (Action == null)
            {
                Action = new EventLogWriter(); 
            }
            Action.actOnNotification(message);
        }

    public INofificationAction getAction() {
        return Action;
    }

    public void setAction(INofificationAction Action) {
        this.Action = Action;
    }
        
        
    
}
