package DIPExampleOne;

public class Program {

    public static void main(String[] args) {
        //Suppose App pool faced a problem, so
        AppPoolWatcher appPoolWatcher = new AppPoolWatcher();
        appPoolWatcher.notify("Memory overflow");
    }

}
