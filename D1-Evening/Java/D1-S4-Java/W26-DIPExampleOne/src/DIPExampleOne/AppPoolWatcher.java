package DIPExampleOne;

public class AppPoolWatcher {

    // Handle to EventLog writer to write to the logs
    public EventLogWriter writer;

    // This function will be called when the app pool has problem
    public void notify(String message) {
        if (writer == null) {
            writer = new EventLogWriter();
        }
        writer.write(message);
    }

    public EventLogWriter getWriter() {
        return writer;
    }

    public void setWriter(EventLogWriter writer) {
        this.writer = writer;
    }

}
