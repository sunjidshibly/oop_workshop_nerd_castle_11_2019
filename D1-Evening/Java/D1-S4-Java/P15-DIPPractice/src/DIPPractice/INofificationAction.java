package DIPPractice;

public interface INofificationAction {
    void actOnNotification(String message);
    
}
