package LSP;

public class LoanAccount extends Account {

    private double PrincipleAmount;
    private double InterestRate;
    private int NoOfInstallment;

    public void disburse() {
        //
    }

    public double getPrincipleAmount() {
        return PrincipleAmount;
    }

    public void setPrincipleAmount(double PrincipleAmount) {
        this.PrincipleAmount = PrincipleAmount;
    }

    public double getInterestRate() {
        return InterestRate;
    }

    public void setInterestRate(double InterestRate) {
        this.InterestRate = InterestRate;
    }

    public int getNoOfInstallment() {
        return NoOfInstallment;
    }

    public void setNoOfInstallment(int NoOfInstallment) {
        this.NoOfInstallment = NoOfInstallment;
    }
    
    

}
