package Inheritance;

public class Program {

    public static void main(String[] args) {
        Laptop aLaptop = new Laptop();
        aLaptop.setManufacturer("Dell");
        aLaptop.setModelName("XPS 13");
        aLaptop.setRAMSizeInGB(8);
        aLaptop.setProcessorSpeedInGHz(3.5);
        aLaptop.setUnitPrice(80000);
        aLaptop.setCounrtyOfOrigin("USA");
        double netPayeable = aLaptop.getUnitPriceAfterDiscount(5);

        System.out.println(netPayeable);
    }

}
