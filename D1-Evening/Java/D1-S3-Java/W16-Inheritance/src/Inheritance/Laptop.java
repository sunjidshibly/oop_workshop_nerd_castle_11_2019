package Inheritance;

public class Laptop extends ElectronicProduct{

    private String ModelName;
    private int RAMSizeInGB;
    private double ProcessorSpeedInGHz;

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String ModelName) {
        this.ModelName = ModelName;
    }

    public int getRAMSizeInGB() {
        return RAMSizeInGB;
    }

    public void setRAMSizeInGB(int RAMSizeInGB) {
        this.RAMSizeInGB = RAMSizeInGB;
    }

    public double getProcessorSpeedInGHz() {
        return ProcessorSpeedInGHz;
    }

    public void setProcessorSpeedInGHz(double ProcessorSpeedInGHz) {
        this.ProcessorSpeedInGHz = ProcessorSpeedInGHz;
    }
    
    
    

}
