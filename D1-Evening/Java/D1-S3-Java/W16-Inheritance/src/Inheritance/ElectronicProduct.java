package Inheritance;

public class ElectronicProduct {

    private String Manufacturer;
    private String CounrtyOfOrigin;
    private double UnitPrice;

    public double getUnitPriceAfterDiscount(double discountPercent) {
        return UnitPrice - (UnitPrice * discountPercent) / 100;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getCounrtyOfOrigin() {
        return CounrtyOfOrigin;
    }

    public void setCounrtyOfOrigin(String CounrtyOfOrigin) {
        this.CounrtyOfOrigin = CounrtyOfOrigin;
    }

    public double getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(double UnitPrice) {
        this.UnitPrice = UnitPrice;
    }
    
    

}
