package ImplementingIComparable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Program {

    public static void main(String[] args) {
        //List<double> prices = new ArrayList();
        //prices.add(120.50);
        //prices.add(195.00);
        //prices.add(110.50);
        //List<double> sortedList = prices.stream().sorted().collect(Collectors.toList());
        //for(double aPrice : prices)
        //{
        //    System.out.println(aPrice);
        //}

        List<Book> books = new ArrayList<>();
        books.add(new Book("Android for professionals", 350));
        books.add(new Book("OOP by examples", 475));
        books.add(new Book("ASP.Net Core", 450));

        List<Book> sortedList = books.stream().sorted().collect(Collectors.toList());

        for (Book aBook : sortedList) {
            System.out.println(aBook.getPrice() + " " + aBook.getTitle());
        }

    }
}


