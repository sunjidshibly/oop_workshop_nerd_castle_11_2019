
package ImplementingIComparable;

public class Book implements Comparable{
    private String Title;
    private double Price;

    public Book(String Title, double Price) {
        this.Title = Title;
        this.Price = Price;
    }
    
    
    

    @Override
    public int compareTo(Object bookToCompare)
    {
        Book aBook = (Book) bookToCompare;
        if (this.Price > aBook.Price)
        {
            return 1;
        }
        else if (this.Price < aBook.Price)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    
    
    
    
}
