package UpcastingDowncasting;

public class Program {

    public static void main(String[] args) {
        CheckingAccount chk1 = new CheckingAccount();
        chk1.deposit(450);
        chk1.withdraw(1000000);

        SavingsAccount sv1 = new SavingsAccount();
        sv1.setInterestAmount(9900);
        sv1.deposit(300);
        sv1.withdraw(1000);

        Account acc = sv1; // upcasting
        SavingsAccount sv = (SavingsAccount) acc; // downcasting
        System.out.println(sv.getInterestAmount());
        
    }

}
