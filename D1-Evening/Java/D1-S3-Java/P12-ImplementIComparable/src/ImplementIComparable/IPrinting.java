package ImplementIComparable;

public interface IPrinting {

    void print();
    void cancelPrinting(int jobNo);

}
