package ImplementIComparable;

import java.util.ArrayList;
import java.util.List;

public class Program {

    public static void main(String[] args) {
        Student student1 = new Student();

        student1.setName("Rana");
        student1.setEmail("r@mail.com");
        student1.setRegNo("8723-273");

        Department department = new Department();
        department.setCode("EEE");
        department.setLocation("A Building");
        department.setName("Elec....");

        Course course1 = new Course();
        course1.setCode("EEE-101");
        course1.setTitle("Basic electronics");
        course1.setCredit(3.5);

        List<IBasicInfo> basicInfoList = new ArrayList<>();
        basicInfoList.add(student1);
        basicInfoList.add(department);
        basicInfoList.add(course1);

        for (IBasicInfo basicInfo : basicInfoList) {
            System.out.println(basicInfo.getBasicInfo());
        }

        Student s = new Student();
        IBasicInfo i = s;
        IPrinting p = s;

        List<IPrinting> printList = new ArrayList();
        printList.add(course1);
        printList.add(student1);

        for (IPrinting printing : printList) {
            printing.print();
        }

        //IBasicInfo i = c;
        //i.GetBasicInfo();
        //IPrinting p = c;
        //p.Print();
        //DisplayBasicInfo(student1);
        //DisplayBasicInfo(c);
        //DisplayBasicInfo(d);
    }

    public static void displayBasicInfo(IBasicInfo basicInfo) {
        System.out.println(basicInfo.getBasicInfo());
    }

}
