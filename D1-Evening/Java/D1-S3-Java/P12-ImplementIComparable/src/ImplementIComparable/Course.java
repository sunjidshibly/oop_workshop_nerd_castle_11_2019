package ImplementIComparable;

public class Course implements IBasicInfo, IPrinting{

    private String Title;
    private String Code;
    private double Credit;

    @Override
    public String getBasicInfo() {
        return getTitle() + " " + getCode() + " " + getCredit();
    }

    @Override
    public void print() {
        System.out.println("Printing : Course");
    }

    @Override
    public void cancelPrinting(int jobNo) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public double getCredit() {
        return Credit;
    }

    public void setCredit(double Credit) {
        this.Credit = Credit;
    }

   

    

    

   

}
