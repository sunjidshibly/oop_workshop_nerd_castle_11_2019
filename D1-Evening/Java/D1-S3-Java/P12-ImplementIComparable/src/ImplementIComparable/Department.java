package ImplementIComparable;

public class Department implements IBasicInfo {

    private String Code;
    private String Name;
    private String Location;

    @Override
    public String getBasicInfo() {
        return Location + " " + Name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    

    
        
    

}
