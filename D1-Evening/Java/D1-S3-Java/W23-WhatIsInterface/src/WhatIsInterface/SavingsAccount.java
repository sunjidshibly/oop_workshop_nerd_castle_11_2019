package WhatIsInterface;

public class SavingsAccount implements IBankAccount {

    private String AccountNo;
    private double Balance;
    private double InterestAmount;

    @Override
    public void deposit(double amount) {
        Balance += amount;
    }

    @Override
    public void withdraw(double amount) {
        Balance -= amount;
    }

    public void calcuateInterest() {
        //TODO
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String AccountNo) {
        this.AccountNo = AccountNo;
    }

    public double getBalance() {
        return Balance;
    }

    private void setBalance(double Balance) {
        this.Balance = Balance;
    }

    public double getInterestAmount() {
        return InterestAmount;
    }

    public void setInterestAmount(double InterestAmount) {
        this.InterestAmount = InterestAmount;
    }

}
