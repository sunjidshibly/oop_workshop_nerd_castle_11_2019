package WhatIsInterface;

public interface IBankAccount {

    void deposit(double amount);
    void withdraw(double amount);

}
