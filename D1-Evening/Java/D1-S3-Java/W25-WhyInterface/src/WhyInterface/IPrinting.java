package WhyInterface;

public interface IPrinting {

    void print();
    void cancelPrinting(int jobNo);
}
