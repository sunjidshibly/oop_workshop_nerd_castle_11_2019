package WhyInterface;

public class Student implements IBasicInfo, IPrinting{

    private String Name;
    private String Email;
    private String RegNo;
    private double CGPA;

    @Override
    public String getBasicInfo() {
        return Name + " " + Email + " " + RegNo;
    }

    @Override
    public void print() {
        System.out.println("Printing : Student");
    }

    @Override
    public void cancelPrinting(int jobNo) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getRegNo() {
        return RegNo;
    }

    public void setRegNo(String RegNo) {
        this.RegNo = RegNo;
    }

    public double getCGPA() {
        return CGPA;
    }

    public void setCGPA(double CGPA) {
        this.CGPA = CGPA;
    }

    

}
