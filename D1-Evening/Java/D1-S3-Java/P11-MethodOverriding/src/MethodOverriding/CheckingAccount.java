package MethodOverriding;

public class CheckingAccount  extends Account{

    public CheckingAccount() {
        System.out.println("Checking constructor is called");
    }

    private double ServiceCharge;

    public double getServiceCharge() {
        return ServiceCharge;
    }

    public void setServiceCharge(double ServiceCharge) {
        this.ServiceCharge = ServiceCharge;
    }

}
