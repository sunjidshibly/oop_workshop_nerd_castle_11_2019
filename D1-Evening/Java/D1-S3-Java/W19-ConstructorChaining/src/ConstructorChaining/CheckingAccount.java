package ConstructorChaining;

public class CheckingAccount extends Account {

    public CheckingAccount() {
        System.out.println("Checking constructor is being called");
    }

    private double ServiceCharge;

    public double getServiceCharge() {
        return ServiceCharge;
    }

    public void setServiceCharge(double ServiceCharge) {
        this.ServiceCharge = ServiceCharge;
    }

}
