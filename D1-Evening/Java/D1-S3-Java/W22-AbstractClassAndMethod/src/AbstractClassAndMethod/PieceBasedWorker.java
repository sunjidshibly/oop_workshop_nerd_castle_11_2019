package AbstractClassAndMethod;

public class PieceBasedWorker extends Worker {

    private int NoOfPiecesProduced;
    private double AmountPerPiece;

    @Override
    public double getTotalWage() {
        if (NoOfPiecesProduced <= 500) {
            return NoOfPiecesProduced * AmountPerPiece;
        } else {
            return (NoOfPiecesProduced * AmountPerPiece) * 1.02;
        }
    }

    public int getNoOfPiecesProduced() {
        return NoOfPiecesProduced;
    }

    public void setNoOfPiecesProduced(int NoOfPiecesProduced) {
        this.NoOfPiecesProduced = NoOfPiecesProduced;
    }

    public double getAmountPerPiece() {
        return AmountPerPiece;
    }

    public void setAmountPerPiece(double AmountPerPiece) {
        this.AmountPerPiece = AmountPerPiece;
    }
    
    

}
