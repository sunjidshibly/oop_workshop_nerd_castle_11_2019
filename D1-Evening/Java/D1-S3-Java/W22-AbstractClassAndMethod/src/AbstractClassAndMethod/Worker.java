package AbstractClassAndMethod;

public abstract class Worker {

    private String Name;
    private String ContactNo;

    public abstract double getTotalWage();

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

}
