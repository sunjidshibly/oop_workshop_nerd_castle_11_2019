
package AbstractClassAndMethod;

public class Program {
    public static void main(String[] args) {
        double totalWage = 0;

            TimeBasedWorker tWorker = new TimeBasedWorker();
            tWorker.setName("Latif");
            tWorker.setContactNo("082635-926");
            tWorker.setTotalHoursWorked(120);
            tWorker.setAmountPerHour(200);
            totalWage = tWorker.getTotalWage();
            System.out.println(tWorker.getName() + "\nTotal Wage: " + totalWage);


            PieceBasedWorker pWorker = new PieceBasedWorker();
            pWorker.setName("Jamil");
            pWorker.setContactNo("927-70-027");
            pWorker.setNoOfPiecesProduced(35);
            pWorker.setAmountPerPiece(450);
            totalWage = pWorker.getTotalWage();
            System.out.println(pWorker.getName() + "\nTotal Wage: " + totalWage);

    }
    
}
