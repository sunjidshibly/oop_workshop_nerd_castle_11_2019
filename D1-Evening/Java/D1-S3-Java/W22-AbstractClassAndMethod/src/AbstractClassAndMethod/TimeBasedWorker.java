/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClassAndMethod;

/**
 *
 * @author kamru
 */
public class TimeBasedWorker extends Worker {

    private int TotalHoursWorked;
    private double AmountPerHour;

    @Override
    public double getTotalWage() {
        return TotalHoursWorked * AmountPerHour;
    }

    public int getTotalHoursWorked() {
        return TotalHoursWorked;
    }

    public void setTotalHoursWorked(int TotalHoursWorked) {
        this.TotalHoursWorked = TotalHoursWorked;
    }

    public double getAmountPerHour() {
        return AmountPerHour;
    }

    public void setAmountPerHour(double AmountPerHour) {
        this.AmountPerHour = AmountPerHour;
    }
    
    
    

}
