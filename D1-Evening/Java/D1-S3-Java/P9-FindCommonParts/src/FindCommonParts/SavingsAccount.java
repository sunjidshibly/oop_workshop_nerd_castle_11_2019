package FindCommonParts;

public class SavingsAccount {

    private String AccountName;
    private double AccountBalance;
    private String AccNo;
    private double InterestAmount;

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String AccountName) {
        this.AccountName = AccountName;
    }

    public double getAccountBalance() {
        return AccountBalance;
    }

    public void setAccountBalance(double AccountBalance) {
        this.AccountBalance = AccountBalance;
    }

    public String getAccNo() {
        return AccNo;
    }

    public void setAccNo(String AccNo) {
        this.AccNo = AccNo;
    }

    public double getInterestAmount() {
        return InterestAmount;
    }

    public void setInterestAmount(double InterestAmount) {
        this.InterestAmount = InterestAmount;
    }
    
    
    

}
