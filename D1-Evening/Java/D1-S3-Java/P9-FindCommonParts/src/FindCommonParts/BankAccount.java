package FindCommonParts;

public class BankAccount {

    private String BankAccountNo;
    private String AccountName;
    private double Balance;

    public void deposit(double amount) {
        Balance += amount;
    }

    public void wthdraw(double amount) {
        Balance -= amount;
    }

    public String getBankAccountNo() {
        return BankAccountNo;
    }

    public void setBankAccountNo(String BankAccountNo) {
        this.BankAccountNo = BankAccountNo;
    }

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String AccountName) {
        this.AccountName = AccountName;
    }

    public double getBalance() {
        return Balance;
    }

    public void setBalance(double Balance) {
        this.Balance = Balance;
    }
    
    

}
