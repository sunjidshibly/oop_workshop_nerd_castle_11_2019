package MethodOverriding;

import java.util.ArrayList;
import java.util.List;

public class Program {

    public static void main(String[] args) {
        CheckingAccount chk1 = new CheckingAccount();
        chk1.deposit(450);
        chk1.withdraw(1000000);

        SavingsAccount sv1 = new SavingsAccount("Hasan", "998.028", 4500);
        sv1.setInterestAmount(9900);
        sv1.deposit(300);
        sv1.withdraw(1000);

        List<Account> accountList = new ArrayList();
        accountList.add(sv1);
        accountList.add(chk1);

        for (Account account : accountList) {
            account.withdraw(100);
        }

        Account acc = sv1; // upcasting
        SavingsAccount sv = (SavingsAccount) acc; // downcasting
        System.out.println(sv.getInterestAmount());

        CheckingAccount chk2 = new CheckingAccount();
        CheckingAccount chk3 = new CheckingAccount();

        Print(sv1);
        Print(chk1);

    }

    public static void Print(Account account) {
        System.out.println(account.getNumber());
    }
}
