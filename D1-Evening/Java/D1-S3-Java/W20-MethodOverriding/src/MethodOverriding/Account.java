package MethodOverriding;

public class Account {

    private String Name;
    private String Number;
    private double Balance;

    public Account() {
        System.out.println("Account constructor is called");
    }

    public Account(String Name, String Number) {
        this();
        this.Name = Name;
        this.Number = Number;
    }

    public void deposit(double amount) {
        Balance += amount;
    }

    void withdraw(double amount) {
        Balance -= amount;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String Number) {
        this.Number = Number;
    }

    public double getBalance() {
        return Balance;
    }

    protected void setBalance(double Balance) {
        this.Balance = Balance;
    }

}
