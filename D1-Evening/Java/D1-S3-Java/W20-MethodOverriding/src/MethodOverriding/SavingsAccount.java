package MethodOverriding;

public class SavingsAccount extends Account {

    private double InterestAmount;

    public SavingsAccount(String name, String number, double interestAmount) {
        super(name, number);
        InterestAmount = interestAmount;
    }

    @Override
    public void withdraw(double amount) {
        if (getBalance() - amount >= 0) {
            super.withdraw(amount);
        } else {

        }
    }

    public double getInterestAmount() {
        return InterestAmount;
    }

    public void setInterestAmount(double InterestAmount) {
        this.InterestAmount = InterestAmount;
    }

}
