package WhenToApplyInheritance;

public class CheckingAccount extends Account{

    private double ServiceCharge;

    public double getServiceCharge() {
        return ServiceCharge;
    }

    public void setServiceCharge(double ServiceCharge) {
        this.ServiceCharge = ServiceCharge;
    }

}
