﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W18_UpcastingDowncasting
{
    class CheckingAccount : Account
    {
        public double ServiceCharge { set; get; }
    }
}
