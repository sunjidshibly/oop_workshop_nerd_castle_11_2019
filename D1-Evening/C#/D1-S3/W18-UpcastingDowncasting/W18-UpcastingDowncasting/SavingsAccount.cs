﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W18_UpcastingDowncasting
{
    class SavingsAccount : Account
    {
        public double InterestAmount { set; get; }
    }
}
