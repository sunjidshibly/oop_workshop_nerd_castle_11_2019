﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W20_MethodOverriding
{
    class CheckingAccount : Account
    {
        public CheckingAccount()
        {
            Console.WriteLine("Checking constructor is called");
        }

        public double ServiceCharge { set; get; }
    }
}
