﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W20_MethodOverriding
{
    public class Account
    {
        public Account()
        {
            Console.WriteLine("Account constructor is called");
        }

        public Account(string name, string number) : this()
        {
            Name = name;
            Number = number;
        }

        public string Name { set; get; }
        public string Number { set; get; }
        public double Balance { protected set; get; }

        public void Deposit(double amount)
        {
            Balance += amount;
        }

        public virtual void Withdraw(double amount)
        {
            Balance -= amount;
        }
    }
}
