﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12_ImplementIComparable
{
    class Department : IBasicInfo
    {
        public string Code { set; get; }
        public string Name { set; get; }
        public string Location { set; get; }


        public string GetBasicInfo()
        {
            return Location + " " + Name;
        }
    }
}
