﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12_ImplementIComparable
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student();
            student1.Name = "Rana";
            student1.Email = "r@mail.com";
            student1.RegNo = "8723-273";

            Department department = new Department();
            department.Code = "EEE";
            department.Location = "A Building";
            department.Name = "Elec....";

            Course course1 = new Course();
            course1.Code = "EEE-101";
            course1.Title = "Basic electronics";
            course1.Credit = 3.5;

            List<IBasicInfo> basicInfoList = new List<IBasicInfo>();
            basicInfoList.Add(student1);
            basicInfoList.Add(department);
            basicInfoList.Add(course1);

            foreach (IBasicInfo basicInfo in basicInfoList)
            {
                Console.WriteLine(basicInfo.GetBasicInfo());
            }

            Student s = new Student();
            IBasicInfo i = s;
            IPrinting p = s;


            List<IPrinting> printList = new List<IPrinting>();
            printList.Add(course1);
            printList.Add(student1);

            foreach (IPrinting printing in printList)
            {
                printing.Print();
            }








            //IBasicInfo i = c;
            //i.GetBasicInfo();

            //IPrinting p = c;
            //p.Print();

            //DisplayBasicInfo(student1);
            //DisplayBasicInfo(c);
            //DisplayBasicInfo(d);


        }

        public static void DisplayBasicInfo(IBasicInfo basicInfo)
        {
            Console.WriteLine(basicInfo.GetBasicInfo());
        }


    }
}
