﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12_ImplementIComparable
{
    class Course : IBasicInfo, IPrinting
    {
        public string Title { set; get; }
        public string Code { set; get; }
        public double Credit { set; get; }

        public string GetBasicInfo()
        {
            return Title + " " + Code + " " + Credit.ToString();
        }

        public void Print()
        {
            Console.WriteLine("Printing : Course");
        }

        public void CancelPrinting(int jobNo)
        {
            throw new NotImplementedException();
        }
    }
}
