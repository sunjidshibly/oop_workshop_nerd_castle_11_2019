﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W19_ConstructorChaining
{
    class CheckingAccount : Account
    {
        public CheckingAccount()
        {
            Console.WriteLine("Checking constructor is being called");
        }

        public double ServiceCharge { set; get; }
    }
}
