﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace W19_ConstructorChaining
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckingAccount chk1 = new CheckingAccount();
            chk1.Deposit(450);
            chk1.Withdraw(1000000);
            
            SavingsAccount sv1 = new SavingsAccount("Hasan", "998.028", 4500);
            sv1.InterestAmount = 9900;
            sv1.Deposit(300);
            sv1.Withdraw(1000);
            Console.ReadKey();
        }

    }
}
