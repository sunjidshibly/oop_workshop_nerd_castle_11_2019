﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace W22_AbstractClassAndMethod
{
    class Program
    {

        static void Main(string[] args)
        {
            double totalWage = 0;

            TimeBasedWorker tWorker = new TimeBasedWorker();
            tWorker.Name = "Latif";
            tWorker.ContactNo = "082635-926";
            tWorker.TotalHoursWorked = 120;
            tWorker.AmountPerHour = 200;
            totalWage = tWorker.GetTotalWage();
            Console.WriteLine(tWorker.Name + "\nTotal Wage: " + totalWage);


            PieceBasedWorker pWorker = new PieceBasedWorker();
            pWorker.Name = "Jamil";
            pWorker.ContactNo = "927-70-027";
            pWorker.NoOfPiecesProduced = 35;
            pWorker.AmountPerPiece = 450;
            totalWage = pWorker.GetTotalWage();
            Console.WriteLine(pWorker.Name + "\nTotal Wage: " + totalWage);

            Console.ReadKey();
        }
    }
}
