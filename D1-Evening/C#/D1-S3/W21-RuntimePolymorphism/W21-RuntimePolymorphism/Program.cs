﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace W21_RuntimePolymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckingAccount chk1 = new CheckingAccount();
            chk1.Deposit(450);
            chk1.Withdraw(1000000);
            
            SavingsAccount sv1 = new SavingsAccount("Hasan", "998.028", 4500);
            sv1.InterestAmount = 9900;
            sv1.Deposit(300);
            sv1.Withdraw(1000);

            List<Account> accountList = new List<Account>();
            accountList.Add(sv1);
            accountList.Add(chk1);

            foreach (Account account in accountList)
            {
                account.Withdraw(100); // Rumtime polymorphism
            }

            Print(sv1);
            Print(chk1);

            Console.ReadKey();
        }

        public static void Print(Account account) //Runtime polymorphism
        {
            Console.WriteLine(account.Number);
        }
    }
}
