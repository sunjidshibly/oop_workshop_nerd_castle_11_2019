﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W21_RuntimePolymorphism
{
    class SavingsAccount : Account
    {
        public SavingsAccount(string name, string number, double interestAmount) :base(name, number)
        {
            InterestAmount = interestAmount;
        }

        public double InterestAmount { set; get; }

        public override void Withdraw(double amount)
        {
            if (Balance - amount >= 0)
            {
                base.Withdraw(amount);
            }
            else
            {
                
            }
        }
    }
}
