﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W17_WhenToApplyInheritance
{
    public class Account
    {

        public string Name { set; get; }
        public string Number { set; get; }
        public double Balance { protected set; get; }

        public void Deposit(double amount)
        {
            Balance += amount;
        }

        public void Withdraw(double amount)
        {
            Balance -= amount;
        }
    }
}
