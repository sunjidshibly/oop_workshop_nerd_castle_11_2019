﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W17_WhenToApplyInheritance
{
    class CheckingAccount : Account
    {
        public double ServiceCharge { set; get; }
    }
}
