﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace W17_WhenToApplyInheritance
{
    class Program
    {
        static void Main(string[] args)
        {   
            SavingsAccount sv1 = new SavingsAccount();
            sv1.Name = "Hasan";
            sv1.Number = "Sv-02562";
            sv1.InterestAmount = 340;
            sv1.Deposit(300);
            sv1.Withdraw(1000);

            CheckingAccount chk1 = new CheckingAccount();
            chk1.Number = "Chk-9275";
            chk1.Name = "Rimon";
            chk1.ServiceCharge = 34000;
            chk1.Deposit(10002);
            chk1.Withdraw(4000);
        }
    }
}
