﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W25_WhyInterface
{
    class Student : IBasicInfo, IPrinting
    {
        public string Name { set; get; }
        public string Email { set; get; }
        public string RegNo { set; get; }
        public double CGPA { set; get; } 

        public string GetBasicInfo()
        {
            return Name + " " + Email + " " + RegNo;
        }

        public void Print()
        {
            Console.WriteLine("Printing : Student");
        }

        public void CancelPrinting(int jobNo)
        {
            throw new NotImplementedException();
        }
    }
}
