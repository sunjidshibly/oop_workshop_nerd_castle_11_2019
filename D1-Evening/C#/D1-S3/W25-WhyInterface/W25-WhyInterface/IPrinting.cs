﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W25_WhyInterface
{
    interface IPrinting
    {
        void Print();
        void CancelPrinting(int jobNo);
    }
}
