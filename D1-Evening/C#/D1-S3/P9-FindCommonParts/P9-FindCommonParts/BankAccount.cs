﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_FindCommonParts
{
    class BankAccount
    {
        public string BankAccountNo { set; get; }
        public string AccountName { set; get; }
        public double Balance { set; get; }
        public void Deposit(double amount)
        {
            Balance += amount;
        }
        public void Wthdraw(double amount)
        {
            Balance -= amount;
        }
    }
}
