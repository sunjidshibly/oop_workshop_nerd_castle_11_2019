﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9_FindCommonParts
{
    class SavingsAccount
    {
        public string AccountName { set; get; }
        public double AccountBalance { set; get; }
        public string AccNo { set; get; }
        public double InterestAmount { set; get; }
    }
}
