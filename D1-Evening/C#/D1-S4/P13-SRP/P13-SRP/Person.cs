﻿using System;
namespace P13_SRP
{
    class Person
    {
        public string FirstName { set; get; }
        public string MiddleName { set; get; }
        public string LastName { set; get; }
        private string fullName;

        public void GetMyFullName()
        {
            fullName = FirstName + " " + MiddleName + " " + LastName;
            Console.WriteLine(fullName);
        }

        public void GetFullReverseName()
        {
            string reverseName = "";
            for (int index = fullName.Length - 1; index >= 0; index--)
            {
                reverseName += fullName[index];
            }
            Console.WriteLine(reverseName);
        }

        public void SaveDataInTextFile()
        { 
            // Code for savings data in text file 
        }


    }
}
