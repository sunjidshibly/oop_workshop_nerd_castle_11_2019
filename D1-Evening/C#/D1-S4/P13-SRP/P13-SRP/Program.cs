﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P13_SRP
{
    class Program
    {
        static void Main(string[] args)
        {
            Person aPerson = new Person();
            aPerson.FirstName = "Fahim";
            aPerson.MiddleName = "Hasan";
            aPerson.LastName = "Rana";

            aPerson.SaveDataInTextFile();
            aPerson.GetMyFullName();
            aPerson.GetFullReverseName();
            Console.ReadKey();
        }
    }
}
