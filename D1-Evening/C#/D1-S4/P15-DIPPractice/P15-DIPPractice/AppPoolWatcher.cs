﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P15_DIPPractice
{
    class AppPoolWatcher
    {
        public INofificationAction Action {set;get;}

        // This function will be called when the app pool has problem
        public void Notify(string message)
        {
            if (Action == null)
            {
                Action = new EventLogWriter(); 
            }
            Action.ActOnNotification(message);
        }
    }
}
