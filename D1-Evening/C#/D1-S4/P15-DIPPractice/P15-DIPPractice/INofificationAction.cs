﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P15_DIPPractice
{
    public interface INofificationAction
    {
         void ActOnNotification(string message);
    }
}
