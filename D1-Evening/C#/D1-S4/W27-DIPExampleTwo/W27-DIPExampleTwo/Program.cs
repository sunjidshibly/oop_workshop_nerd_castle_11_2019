﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W27_DIPExampleTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            AppPoolWatcher poolWatcher = new AppPoolWatcher();
            poolWatcher.Action = new EmailSender();
            poolWatcher.Notify("Memory overflow");

            poolWatcher.Action = new EventLogWriter();
            poolWatcher.Notify("Too many applictions");


        }
    }
}
