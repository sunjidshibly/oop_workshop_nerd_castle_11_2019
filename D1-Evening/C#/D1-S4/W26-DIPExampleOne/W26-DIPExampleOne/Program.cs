﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W26_DIPExampleOne
{
    class Program
    {
        static void Main(string[] args)
        {
            //Suppose App pool faced a problem, so
            AppPoolWatcher appPoolWatcher = new AppPoolWatcher();
            appPoolWatcher.Notify("Memory overflow");
        }
    }
}
