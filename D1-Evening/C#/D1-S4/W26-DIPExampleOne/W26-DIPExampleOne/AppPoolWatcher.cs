﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W26_DIPExampleOne
{
    class AppPoolWatcher
    {
        // Handle to EventLog writer to write to the logs
        public EventLogWriter writer { set; get; }

        // This function will be called when the app pool has problem
        public void Notify(string message)
        {
            if (writer == null)
            {
                writer = new EventLogWriter();
            }
            writer.Write(message);
        }
    }
}
