﻿using System;

namespace P14_LSP
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckingAccount chk1 = new CheckingAccount();
            chk1.Deposit(450);
            chk1.Withdraw(1000000);
            
            SavingsAccount sv1 = new SavingsAccount();
            sv1.InterestAmount = 9900;
            sv1.Deposit(300);
            sv1.Withdraw(1000);

            LoanAccount ln1 = new LoanAccount();
            ln1.Withdraw(1000); // very harmful
        }

    }
}
