﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14_LSP
{
    class LoanAccount : Account
    {
        public double PrincipleAmount {set;get;}
        public double InterestRate { set; get; }
        public int NoOfInstallment { set; get; }
        public void Disburse()
        {
            //
        }
    }
}
