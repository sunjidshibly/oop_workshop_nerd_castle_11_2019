<?php


interface NotificationAction
{
    public function actOnNotification($message);
}