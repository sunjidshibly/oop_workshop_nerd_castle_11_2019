<?php
include_once "Person.php";
include "PrintData.php";
include "SaveData.php";

$aPerson = new Person();
$aPerson->setFirstName("Fahim");
$aPerson->setMiddleName("Hasan");
$aPerson->setLastName("Rana");

$print_person = new SaveData();
$print_person->saveDataInTextFile($aPerson);

$person_full_name = $aPerson->getMyFullName();
$person_reverse_name = $aPerson->getFullReverseName();

$print = new PrintData();
$print->printingtData($person_full_name);

$print->printingtData($person_reverse_name);
