<?php
require_once  "Account.php";

$account1 = new Account();

$account1->balance = 0;
$account1->account_holder_namel = "Sunjid";
$account1->account_number = "SA123456";
$account1->account_type = "Saving";

$account1->deposit(500);
echo "Account balance: ".$account1->balance;
echo "<br>";
$account1->withdraw(100);
echo "Account balance: ".$account1->balance;