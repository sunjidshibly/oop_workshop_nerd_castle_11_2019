<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 22-Nov-19
 * Time: 10:32 AM
 */
class Account
{
    public $account_number;
    public $balance;
    public $account_holder_namel;
    public $account_type;

    public function deposit($given_money){
        $this->balance += (int)$given_money;
    }

    public function withdraw($given_money){
        $this->balance -= (int)$given_money;
    }
}