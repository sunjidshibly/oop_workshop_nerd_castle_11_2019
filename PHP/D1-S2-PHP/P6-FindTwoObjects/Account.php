<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 22-Nov-19
 * Time: 3:49 PM
 */
class Account{
    private $account_number;
    private $type;
    private $info;
    private $balance;

    public function __construct($account_number,$type,$info)
    {
        $this->account_number = $account_number;
        $this->type = $type;
        $this->info = $info;
        $this->balance = 0;

    }

    public function deposit($amount):void
    {
        $this->balance =+$amount;
    }
    public function withdraw($amount):void
    {
        $this->balance -= $amount;
    }
    public function getBalance()
    {
        return $this->balance;
    }

}