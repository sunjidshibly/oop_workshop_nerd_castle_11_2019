<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 22-Nov-19
 * Time: 3:50 PM
 */
class Customer{
    private $name;
    private $email;
    private $contact;
    private $number;

    public function __construct($name,$email,$contact,$number)
    {
        $this->name = $name;
        $this->email = $email;
        $this->contact = $contact;
        $this->number = $number;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getEmail()
    {
        return $this->email;
    }
}