<?php
class Employee
{
    private $id;
    private $name;
    private $email;

    //Following three data is related to employee salary
    //Instead of repeating these, we should use Salary class here.
    //So, Replace these using Salary class and also getter and setter
    private $salary;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }
    public function getName()
    {
        return $this->name;
    }


    public function setName($name): void
    {
        $this->name = $name;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }
    public function setSalary($salary):void
    {
        $this->salary = $salary;
    }
    public function getSalary()
    {
        return $this->salary;
    }

}