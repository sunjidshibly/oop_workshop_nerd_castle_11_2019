﻿namespace NullObjectSample
{
    class Employee
    {
        public string EmployeeCode { set; get; }
        public double AmountToBePaid { set; get; }
        public Employee(string employeeCode, double amountToBePaid)
        {
            EmployeeCode = employeeCode;
            AmountToBePaid = amountToBePaid;
        }

        public string Pay()
        {
            return "Paid";
        }
    }
}
