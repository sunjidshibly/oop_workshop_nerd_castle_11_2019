﻿using System;

namespace NullObjectSample
{
    class Program
    {
        static void Main(string[] args)
        {
            DB employeeDb =new DB();

            Employee anEmployee = employeeDb.GetEmployee("006542");
            if (anEmployee != null)
            {
                Console.WriteLine(anEmployee.Pay());
            }
            else
            {
                Console.WriteLine("Employee is not found");
            }
            Console.ReadKey();
        }
    }
}
